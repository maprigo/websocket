from pymongo import MongoClient, errors
import asyncio
import websockets
import json
import sys

config = {
    "username": "root",
    "password": "Secret",
    "server": "mongo",
}

connector = "mongodb://{}:{}@{}".format(config["username"], config["password"], config["server"])

# use a try-except indentation to catch MongoClient() errors
try:
    # try to instantiate a client instance
    client = MongoClient(connector)
    db = client["demo"]

    # print the version of MongoDB server if connection successful
    print ("server version:", client.server_info()["version"])

    # get the database_names from the MongoClient()
    database_names = client.list_database_names()

except errors.ServerSelectionTimeoutError as err:
    # set the client and DB name list to 'None' and `[]` if exception
    client = None
    database_names = []

    # catch pymongo.errors.ServerSelectionTimeoutError
    print ("pymongo ERROR:", err)

print ("\ndatabases:", database_names)
    


def store(event):
    events = db.events
    res = events.insert_one(event)

def count():
    edit_count =  db.events.find({'action':'edit'}).count()
    create_count =  db.events.find({'action':'create'}).count()
    hit_count =  db.events.find({'action':'hit'}).count()
    total = edit_count + create_count + hit_count
    if total == 0 :
        total=1 
    create_percent = (create_count/total) * 100
    edit_percent = (edit_count/total) * 100
    hit_percent = (hit_count/total) * 100
    data = [
        {
    "action": "Create",
    "value": create_count,
    "percent": int(create_percent),
    "color": "#00a2ee"
  },{
    "action": "Edit",
    "value": edit_count,
    "percent": int(edit_percent),
    "color": "#000000"
  },
  {
    "action": "Hit",
    "value": hit_count,
    "percent": int (hit_percent),
    "color": "#fbcb39"
  }
]
    with open('./data/test.json', 'w') as outfile:
        json.dump(data, outfile)
    return str(create_count)

async def hatnote():
    async with websockets.connect("ws://wikimon.hatnote.com:9000") as ws:
        while True:
            event = await ws.recv()
            store(json.loads(event))
            count()
            print(json.loads(event))
loop = asyncio.get_event_loop()
loop.run_until_complete(hatnote())
