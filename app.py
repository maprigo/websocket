from pymongo import MongoClient, errors
import asyncio
import websockets
import json
import sys
from flask import Flask , jsonify
from flask import render_template

config = {
    "username": "root",
    "password": "Secret",
    "server": "mongo",
}

connector = "mongodb://{}:{}@{}".format(config["username"], config["password"], config["server"])

# use a try-except indentation to catch MongoClient() errors
try:
    # try to instantiate a client instance
    client = MongoClient(connector)
    db = client["demo"]

    # print the version of MongoDB server if connection successful
    print ("server version:", client.server_info()["version"])

    # get the database_names from the MongoClient()
    database_names = client.list_database_names()

except errors.ServerSelectionTimeoutError as err:
    # set the client and DB name list to 'None' and `[]` if exception
    client = None
    database_names = []

    # catch pymongo.errors.ServerSelectionTimeoutError
    print ("pymongo ERROR:", err)

print ("\ndatabases:", database_names)
    

def store_mock():
    events = db.events
    event_mock = {
    "action": "edit",
    "change_size": "null",
    "flags": "null",
    "hashtags": [],
    "is_anon": "false",
    "is_bot": "false",
    "is_minor": "false",
    "is_new": "false",
    "is_unpatrolled": "false",
    "mentions": [],
    "ns": "Special",
    "page_title": "Special:Log/newusers",
    "parsed_summary": "New user account",
    "section": "",
    "summary": "New user account",
    "user": "Bodduluri pavan kumar"
    } 
    res = events.insert_one(event_mock)
    print(f"One event: {res.inserted_id}")
    res_count = events.count()
    print(f"Count: {res_count}")
    res_collection = db.events.find()
    print(f"collection: {list(res_collection)}")



def store(event):
    events = db.events
    res = events.insert_one(event)

app = Flask(__name__)


@app.route("/show")
def show():
    res = db.events.find()
    return render_template("show_events.html", events=res)

@app.route("/count")
async def count_view():
    edit_count =  db.events.find({'action':'edit'}).count()
    create_count =  db.events.find({'action':'create'}).count()
    hit_count =  db.events.find({'action':'hit'}).count()
    data = [{
    "action": "Edit",
    "value": edit_count,
    "color": "#000000"
  },
  {
    "action": "Create",
    "value": create_count,
    "color": "#00a2ee"
  },
  {
    "action": "Hit",
    "value": hit_count,
    "color": "#fbcb39"
  }
]
    with open('./data/test.json', 'w') as outfile:
        json.dump(data, outfile)
    return str(create_count)

@app.route("/empty")
async def empty():
    db.events.delete_many({})
    return "DB Empty"
    
@app.route("/live")
async def live():
    return render_template('live.html')

@app.route("/")
async def chart():
    return render_template('static.html')

@app.route("/get-data")
async def get_data():
    data = {}
    with open('./data/test.json', 'r') as myfile:
        info = myfile.read()
        data = json.loads(info)
    return jsonify(data)

def count():
    edit_count =  db.events.find({'action':'edit'}).count()
    create_count =  db.events.find({'action':'create'}).count()
    hit_count =  db.events.find({'action':'hit'}).count()
    data = [{
    "action": "Edit",
    "value": edit_count,
    "color": "#000000"
  },
  {
    "action": "Create",
    "value": create_count,
    "color": "#00a2ee"
  },
  {
    "action": "Hit",
    "value": hit_count,
    "color": "#fbcb39"
  }
]
    with open('./data/test.json', 'w') as outfile:
        json.dump(data, outfile)
    return str(create_count)

async def hatnote():
    async with websockets.connect("ws://wikimon.hatnote.com:9000") as ws:
        while True:
            event = await ws.recv()
            store(json.loads(event))
            # count()
# loop = asyncio.get_event_loop()
# loop.run_until_complete(hatnote())
if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=True)